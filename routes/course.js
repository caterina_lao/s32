const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");


router.post("/", (req, res) => {
	courseController.addCourse(req.body).then(resultFromController =>
		res.send(resultFromController))
})

//1st Solution
/*router.get("/addCourse", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	courseController.addCourse({userId: req.body.id}).then(resultFromController => res.send(resultFromController))
})*/

//2nd Solution
router.post("/", (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	courseController.addCourse(userData,req.body).then(resultFromController =>
		res.send(resultFromController))
})

//Retrieve all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
});

//Retrieve all active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

//Retrieve specific course

router.get("/:courseId", (req, res) => {
	console.log(req.params)
	console.log(req.params.courseId)


	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

//Update a course

router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

//mini activity s35
router.put("/:courseId/archive", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization);

	
	courseController.updateCourseStatus(user, req.params, req.body).then(resultFromController => res.send(resultFromController));

	

})

module.exports = router;