const Course = require("../models/Course");

//2nd solution
module.exports.addCourse = async (user, reqBody) => {
	if(user.isAdmin) {
	let newCourse = new Course ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
} else {
	return("Not Authorized");
}

}

//Retrieve all the Courses
module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result;
	});
};

// Retrieve all active courses

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

//Retrieve specific course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		// return result;
		let getCourse = {
			name:result.name,
			price: result.price
		}
		return getCourse
	});
};

//Update a course

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	// Syntax:
		// findByIdAndUpdate(document, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

//mini activity

module.exports.updateCourseStatus = async (user, reqParams, reqBody) => {
	if(user.isAdmin){
		let updateActiveField = {
			isActive: reqBody.isActive
		}

		return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return("Not Authorized");
	}
}
